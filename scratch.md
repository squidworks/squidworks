# AutomataKit

automatakit
automatickit 

# Ambitions for this page 

 - landing page for all users, consider FAB14 workshop landing here 
  - is machine / robot control for fungible machines 
  - show video examples Reich, MoControl 
 - explain scheme, idea, statelessness and event-basedness 

 - practicum
  - how to mkterminal, what is
  - how to atk api javascript
  - explain example application entry point
  - explain example C code


# Global Commands

All members of the automatakit hardware family will accept the following commands:

### Test

Keycode: **127**
 - to test networking, this will reply with a payload packet containing ```127, 12, 24, 48``` and will toggle an LED on the board

### Reset

Keycode: **128**
 - issues a software microcontroller reset
 - if the microcontroller is already hung up, this will not work


XX Is a collection of hardware, firmware and software that lets you build machines in the same way we build software: quickly, with great joy, and with rapid reconfiguration of resources.

With XX, all of the motors, sensors, and end effectors in a machine are connected via a low-latency network that we call [APA](link). An API allows us to interface with this network to build applications for machine control. 

```JavaScript
Stepper.trapezoid(entry,steps,rate,exit);
Stepper.onPacket(packet){
	// on return packet
}

DCRotary.setPos(pos);
DCRotary.onPacket(packet){
	// 
}

// all have
Endpoint.sendPacket(packet);
Endpoint.onPacket(packet){
	// returning
}
```

Entry points to the network are through the [mksocketserver](link) and [mkterminal](link) interfaces. These open hardware ports on a host computer to send and receive messages across the network.

Endpoints are hardware objects that receive control commands, and execute them faithfully: move motors, read sensors, heat things, control positions and currents, etc. 

## APA Networking
 
 - clean nomenclature: 'routers' are 'nodes' and can be just-routers or endpoints, etc, anything that lives on the network is a node

### The Packet

*Asynchronous Packet Automata* is a networking scheme from Neil Gershenfeld, wherein packets traverse a network not based on destination addresses, but on designated routes. In Technical Networking Language, is is a source-routed port-forwarding scheme, which means that routing is done at the source of the packet, and upon arriving at a position in the network, packets are forwarded along another port, or consumed at that position, based on the route contained in the packet. As the packet traverses the network, pieces of the route are 'consumed' and replaced with a reversed route, such that the destination knows how to return the packet.

The structure of a packet is typically broken into bytes (8-bit long 'words') and is structured like this:

![apa-packet](/images/apa-packet-structure.png)

The 1st byte of any packet denotes the length of the complete packet, in bytes, and is used to parse packets arriving at nodes. For example, when a node starts receiving a packet, it takes the first byte arriving to be the length, and counts bytes until the end is reached, when it can then act on the packet. 

### Routing

Bytes in the route-header section of the packet designate the order of ports the packet should be forwarded on. When the packet arrives at a new network node, the node shifts-out the last port, and adds to the tail of the route-header the port that the packet arrived on, effectively adding return information to the route. The 'pointer' (we designate the byte representing 254, or ^ diagramatically), denotes the packet's current position in the route. 

We can see an example packet traversal here:

![apa-packet-traverse](/images/apa-packet-traverse.png)

![apa-packet-route-shift](/images/apa-packet-shift.png)

Because of the route-header shifting, the 1st byte in the route-header always denotes the port number which the packet should leave the current node on. If the 1st byte in the route-header is the pointer (^ or 254) the packet is at its destination and should be consumed by that node. At this point, the tail of the packet-header contains a route which, when reversed, forms a route back to it's original source. 

![apa-packet-reply](/images/apa-packet-reply.png)

### The Physical Layer 

APA does not designate a particular Physical Layer (in Networking Nomenclature, referred to as a 'PHY', or Layer 2). AutomataKit uses a microcontroller's UART peripheral for the PHY layer, but we are expanding to use FPGAs and a per-bit token-passing scheme, as well as exploring wireless links.  

### Hardware Implementation

The network nodes implemented in AutomataKit use a UART Physical Layer. This means that network packets arrive on a per-byte basis, and are parsed into packets in the microcontroller's C code (firmware). 

... etc 


## Endpoints

Endpoints are hardware...

Global API

**127** test, turns on a light and returns the packet

**128** reset, resets the board

**129** setpos,

**130** getpos,

**131** trapezoid,

### MKRouter

### MKStepper17

### MKStepper23

### MKBLDCDriver

### MKBreadBoardBoard

### MKSmallRotary

## Applications

 - example applications do:
  - mocontrol acceleration
  - dynamic control
  - feedback?



# Global Notes

## W/R/T ATSAMD51

