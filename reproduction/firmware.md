# AutomataKit Reproduction / Firmware Programming

## Building Firmware 

To build, program and debug firmware, I use Atmel Studio 7. Code for all of the endpoints is kept in their respective repositories, under the embedded/ directory.

While the code is kept in the repositories, supporting libraries and build tools are not - we need Atmel Studio to do this for us.

In Atmel Studio, start a new project, select 'GCC Executable Project' and set the location to be the sub-directory where the .c and .h files are kept.

![atsuo-1](/images/atstudio-01.png)

On the next page, select the Device to be an ATXMEGA256A3U - this is the microcontroller most of the ATK endpoints run. This image shows a different micro, no worries. **ATXMEGA263A3U**

![atsuo-2](/images/atstudio-02.png)

Now Atmel Studio should be set up, but we need to have it include the existing .c and .h files. 

In the Atmel Studio 'Solution Explorer' (might not be open, go to View -> Solution Explorer), right-click on the Solution (mkembedded-example in this case) and do Add -> Existing Item

![atsuo-3](/images/atstudio-03.png)

Now select all of the .c and .h files from the example code.

![atsuo-4](/images/atstudio-04.png)

Your Atmel Studio environment and project embedded director should look something like this:

![atsuo-5](/images/atstudio-05.png)

Now we should be ready to build the example:

![atsuo-6](/images/atstudio-06.png)

## Loading Firmware 

Now that we're building in ATSUO, we can program the boards. The headers are for the 'PDI' interface, - Programming Data Interface. This is ATMEL Proprietary, boo! 

![img](/images/programming.jpg)

We need to power the board up before we can program it. One LED on each board (labelled, if they are, pwr) is just a straight shot connection between +3v3 and GND, so if power on the board is OK, this will light up. 

To load a program onto the device, connect the programmer (in this case, I'm using an Atmel ICE) and do Ctrl + Alt + F5, or hit the little 'play' button underneath the 'window' menu item.

If you get an error saying you don't have a tool connected, hit OK and select the Atmel-ICE from the menu that appears. Otherwise, to set up a programming tool in Atmel Studio go to 'Tools -> Device Programming' or right-click on the solution and open the properties. From there you can select a tool in the 'Tool' tab on the left.

Also! Make sure the AVR-ICE / PDI cable is connected to the ICE port that says 'AVR' on it, not 'SAM'. The SAM port is for ARM-Cortex targets, AVR for AT-everything-else.

If it programs, you will probably see the 'STL' light start to flash. Good news!