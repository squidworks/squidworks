# Node and Raspberry Pi

It's nice to serve node projects on a raspberry pi, and we specifically would like to (eventually) bundle build tools / dev tools etch into 'head' machines like this, so, the guide, as I imagine I will do this again in the future / ask others to do it.

First, setup your raspberry pi. You can effectively do all of this with SSH, but I always just get a keyboard and mouse and do it au-manuel.

Once you have the pi setup, you'll want to install node on the pi. The best way to do this, I think, is using NVM: the node version manager. We install NVM first, then it installs node and npm.

#### Install NVM

[NVM install is easy on the command line](https://github.com/nvm-sh/nvm#installation-and-update)

Now we can use NVM to install / update node and npm. Meta! How about NVMVM?

That's about it I suppose!
