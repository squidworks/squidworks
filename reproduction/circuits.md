# Manufacturing SMT Boards 

Beyond circuit milling and bread-boarding, designing and assembling high quality PCBs is becoming easier and easier. This is a brief document for people interested in venturing into surface mount / reflow circuit board assembly, that assumes you have basic knowledge of PCB design.

## 1. Design Exports

First up is making sure you design a board that can be fabricated. Most fabricators specify a 'trace/space' capability: this is the *minimum width of a trace* / the *minimum space between traces* - trace/space changes width copper weight (a measure of how thick the copper on the board is, units are in oz/ft^2). Here's a of the units I use 'to be safe'. 

One 'mil' is 0.001" 

| Copper Weight | Minimum Trace Size | Minimum Space Size | Minimum Hole Size |
| --- | --- | --- | --- |
| 1oz | 6mil / 0.15mm | 6mil / 0.15mm | 0.3mm |
| 2oz | 8mil / 0.2mm | 8mil / 0.2mm | 0.3mm |

You'll also need to keep track of a few extra layers. Here are there names and contents:

**Top** - Top Copper  
**Bottom** - Bottom Copper  
**Dimension** - The Cutout Layer  
**tStop / bStop** - Top and Bottom Solder Masks (as in, stop the solder mask on these areas)   
**tCream / bCream** - Top and Bottom Solder Stencil Openings  
**tPlace / bPlace** - Silkscreens (text / drawings)  

Once you have a board d-signed (I use Eagle but have been recently thinking about switching back to KiCad), you can export what are called 'gerber' files to a .zip folder. Eagle has a button in the top-right of the window for this, that looks like a tiny factory. You can also do ```File -> Cam Processor```. 

To make this easy, [Sparkfun has a Cam File (a set of instructions for the Cam Processor) that you can use, from this link.](https://learn.sparkfun.com/tutorials/using-eagle-board-layout/generating-gerbers)


## 2. Board Manufacture

Ordering boards should be straightforward, a few services include:

- [JLCPCB](https://jlcpcb.com/) 
- [OHS Park](https://oshpark.com/)  
- [PCB:NG](https://www.pcb.ng/)  
- [PCBWAY](https://www.pcbway.com/)  

Depending on the manufacturer's uptake system, you'll have to enter some basic information: board size, desired pcb thickness (1.6mm thick FR4 is standard), copper weight (1oz is normal, use thicker for higher current requirements, [consult a trace width calculator](https://www.4pcb.com/trace-width-calculator.html)), solder mask color, stencil color, etc.

Order boards, and I really recommend ordering a solder stencil. I get stencils 'with framework' (meaning they arrive in an aluminum frame, makes them easy to handle) and in the ~ standard 370x430mm size.

## 3. Solder Stenciling

This is the magic: we can put all of the solder on at once, tweezer components into place, and solder them all at once. And, reflow makes it easy to solder tiny pads.

This is also kind-of where I stop being able to add value. I'm going to point you to [Sparkfun's Great Tutorial on SMT Assembly](https://learn.sparkfun.com/tutorials/electronics-assembly). 

[Here's that link again](https://learn.sparkfun.com/tutorials/electronics-assembly)  
[Adafruit's also documents this stuff](https://learn.adafruit.com/smt-manufacturing?view=all#overview)  

## 4. Part Placing

Sparkfun uses a pick and place, but an equally valid way-of-getting-parts-onto-boards is to tweezer them into place. I get down with a microscope and a tiny tray of the parts I'll need for the specific circuit, and tweezer away. Reflow'ed components don't need to be expertly placed, the solder paste's surface tension during reflow will do some local error correction. [Here's a video of that happening](https://youtu.be/qyDRHI4YeMI?t=1421).

## 5. Reflow ! 

Now, you'll also need (or want) a reflow oven. I used the [controleo](http://www.whizoo.com/) to convert a toaster oven into a reflow oven. If you or your fab-community is not stoked about investing too much time into this, you can also do some [frying-pan reflow](https://www.youtube.com/watch?v=3drirgNktQg).

# AutomataKit Reproduction / Circuit Design and Manufacturing

If you'd like to make your own hardware for the ATK Network, I recommend using the [ATKBreadBoardBoard](https://gitlab.cba.mit.edu/jakeread/atkbreadboardboard) whose documentation also includes a 'daughter' board starter - this will let you plug some new circuitry right onto the available XMEGA pinouts, and use known-to-work programming, voltage regulation, and network interface hardware. 

![sch01](images/atk-phy-schematic-01.png)
![sch01](images/atk-phy-schematic-02.png)

![cir01](images/atk-phy-circuit-01.png)
![cir02](images/atk-phy-circuit-02.png)

Each AutomataKit project includes circuit schematic and board files, as well as some notes (if I've been feeling verbose) under the /circuit directory, and should include a BOM.